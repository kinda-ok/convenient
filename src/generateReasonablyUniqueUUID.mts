
const _reasonableUuidFunctionGenerator = ()  => {
  let start = 100000000;
  return () => (start++).toString(36);
}

/**
 * A reasonably unique id.
 * Deterministic, it will always return the same id for the same call order.
 * Not suitable for databases, but well unique enough for app elements IDs.
 */
export const generateReasonablyUniqueUUID = _reasonableUuidFunctionGenerator();
