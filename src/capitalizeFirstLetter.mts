import { isFalsy } from "./isFalsy.mjs";

export type CapitalizedString = string

/**
 *
 * Capitalizes the first letter of a string
 * or an array of strings.
 *
 *
 * If passed an array, the function will join them
 * with the `sep` argument.
 *
 * @param str
 * @param [sep] defaults to "."
 */
export const capitalizeFirstLetter = (str: (string | string[]), sep: string = "."): CapitalizedString =>
  isFalsy(str)
    ? ""
    : Array.isArray(str)
    ? str
        .map((s) => capitalizeFirstLetter(s, sep))
        .filter(Boolean)
        .join(sep)
    : str.charAt(0).toUpperCase() + str.slice(1);
