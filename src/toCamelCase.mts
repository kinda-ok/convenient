import { toPascalCase } from "./toPascalCase.mjs";
import { lowercaseFirstLetter } from "./lowercaseFirstLetter.mjs";

type CamelCasedString = string

/**
 *
 * If passed an array, it will be joined with the `sep` argument.
 *
 * @param str
 * @param [sep] defaults to `_`
 */
export const toCamelCase = (str: string | string[], sep = "_"): CamelCasedString =>
  Array.isArray(str)
    ? str.map((s) => toCamelCase(s, sep)).join(sep)
    : lowercaseFirstLetter(toPascalCase(str));
