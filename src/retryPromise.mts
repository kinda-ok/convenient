/**
 * Retries a promise N times, allowing it to fail by silently swallowing
 * errors, until `N` has run out.
 * @param promiseProviderFunc
 * @param max defaults to 5
 * @returns
 */
export function retryPromise<T>(
  promiseProviderFunc: () => Promise<T>,
  max = 5
): Promise<T> {
  if (max <= 0) {
    return promiseProviderFunc()
  }

  let promise: Promise<T> = Promise.reject(new Error())

  for (let i = 0; i < max; i++) {
    promise = promise.catch(promiseProviderFunc)
  }
  return promise
}