import { bernstein } from './utils/bernstein.mjs'
import { catmullRom } from './utils/catmullRom.mjs'
import { factorial } from './utils/factorial.mjs'
import { linear } from './utils/linear.mjs'

export const utils = { bernstein, catmullRom, linear, factorial }

