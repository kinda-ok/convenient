import { isObject } from "./isObject.mjs";

const proto = Object.prototype;
const gpo = Object.getPrototypeOf;

/**
 * check if the argument passed is a plain javascript object (pojo)
 */
export const isPlainObject = (obj: unknown): obj is object => isObject(obj) && gpo(obj) === proto;
