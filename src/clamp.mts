//@ts-check
/**
 * Clamps a number between the two provided boundaries
 * @param max
 * @param min defaults to 0
 */
export const clamp =
  (max: number, min = 0) =>
  (num: number) =>
    Math.min(Math.max(num, min), max)


