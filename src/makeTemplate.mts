/**
 * Returns a template
 */
export const makeTemplate = (templateString: string) => {
  const template = document.createElement("template");
  template.innerHTML = templateString;
  return template;
};

/**
 * Convenience literal to create DOM template elements
 */
export const tmpl = (strings: TemplateStringsArray, ...substitutions: any[]) => {
  const formattedString = strings.reduce(
    (acc, curr, index) => acc + curr + (substitutions[index] || ""),
    ""
  );
  return makeTemplate(formattedString);
};
