/**
 * is true if the global `window` exists
 */
export const isEnvBrowser = typeof window !== 'undefined'


