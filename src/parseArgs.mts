//@ts-check
import { readArgs } from "./readArgs.mjs";
import { toKebabCase } from "./toKebabCase.mjs";
import { clamp } from "./clamp.mjs";
import { keyword, abbreviation, description } from "./readArgs.mjs";

type ParseArgsFunctor<T> = (value: (string | boolean)[], key: string) => T;

type Expander<T> = Record<
	keyword,
	[abbreviation, description | description[], ParseArgsFunctor<T>?]
>;

type ExtractExpanderType<Prop extends [abbreviation, description | description[], ParseArgsFunctor<any>?]> = Prop[2] extends ((...args:any[]) => infer T) ? T : unknown

type Options<Type extends Expander<any>> = {
	[Property in keyof Type]: ExtractExpanderType<Type[Property]>;
};

interface ParseArgsOptions {
	padding?: number;
	descriptionPadding?: number;
}

export const pickFirst = <T,>(arr: T[]) => arr[0];

export const bool =
	(def = false) =>
	(arr: (string | boolean)[], key: string) => {
		if (typeof arr[0] === "undefined") {
			return def;
		}
		return !!pickFirst(arr);
	};

export const number =
	(def = 0, min = 0, max = 99999) =>
	([num]: (string | boolean)[]) => {
		if (!num || num === true) {
			return def;
		}
		return clamp(max, min)(parseInt(num));
	};

export const string =
	(def = "") =>
	([str]: (string | boolean)[]) => {
		if (!str || str === true) {
			return def;
		}
		return str;
	};

export const path = string

/**
 * Maps command line arguments to an object of properties.
 * 
 * The `args` object is expected to start with the executable and path, like regular
 * node `process.argv`. If using this from another context, make sure to add two
 * arguments at the top of the array.
 * 
 * @param args 
 * @param [optionsMap]
 */
export const parseArgs = <E extends Expander<any>>(
	args: string[],
	optionsMap: E,
	{ descriptionPadding = 24, padding = 2 }: ParseArgsOptions = {}
) => {
	const expand = Object.entries(optionsMap).reduce((acc, [keyword, [abbr]]) => {
		acc[abbr] = keyword;
		acc[toKebabCase(keyword)] = keyword;
		acc[keyword] = keyword;
		return acc;
	}, {} as Record<string, string>);
	const { executable, path, file, flags, rest } = readArgs(args, expand);

	const expected = Object.keys(expand)
	const unknown = Object.keys(flags).filter(x => !expected.includes(x))

	const options = {} as Options<E>;
	const optionsDocs = [] as string[];
	optionsMap &&
		Object.entries(optionsMap).forEach(
			([keyword, [abbreviation, description, fn]]) => {
				const keywords = `-${abbreviation}, --${toKebabCase(keyword)}`;
				const body = Array.isArray(description) ? description[0] : description;
				const text = `${keywords.padEnd(descriptionPadding)}${body}`;
				optionsDocs.push(text);
				if (Array.isArray(description)) {
					optionsDocs.push(
						...description
							.slice(1)
							.map((line) => ` `.padEnd(descriptionPadding) + line)
					);
				}
				const key = keyword as keyof Options<E>;
				const value = flags[keyword] ?? [];
				options[key] = fn ? fn(value, keyword) : flags[keyword];
			}
		);
	return { optionsDocs, unknown, options, executable, path, file, flags, rest };
};