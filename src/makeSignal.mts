

export type SignalListener<T> = (args: T) => void

export interface ListenerOptions{
	signal?: AbortSignal,
	once?: boolean
}


export interface SignalWithArguments<T>{
	connect: (fn: SignalListener<T>, options?: ListenerOptions) => () => boolean;
	disconnect: (fn: SignalListener<T>) => boolean;
	emit: (args: T) => false | void;
	disable: () => void;
}

export interface SignalWithoutArgument{
	connect: (fn: SignalListener<undefined>, options?: ListenerOptions) => () => boolean;
	disconnect: (fn: SignalListener<undefined>) => boolean;
	emit: () => false | void;
	disable: () => void;
}

interface SignalMaker{
	(): SignalWithoutArgument
	<T>(initial?:T): SignalWithArguments<T>
}

export type Signal<T = undefined> = T extends undefined ? SignalWithoutArgument : SignalWithArguments<T>


/**
 * Returns an event emitter for a specific signal
 * The initial passed value is optional, discarded, and only used to provide
 * automatic typing where applicable.
 */
export const makeSignal = (<T = undefined,>(_initial?: T) => {
  
  const listeners: Set<SignalListener<T>> = new Set();

  let enabled = true;
  
  const connect = (fn: SignalListener<T>, options = {} as ListenerOptions) => {
    const { once, signal } = options;
    if (once) {
      const _bound = fn;
      fn = (args) => {
        listeners.delete(fn);
        _bound(args);
      };
    }
    listeners.add(fn);
    const _disconnect = () => disconnect(fn);
    signal && signal.addEventListener("abort", _disconnect);
    return _disconnect;
  };

  const disconnect = (fn: SignalListener<T>) => listeners.delete(fn);

  const emit = (args: T) => enabled && listeners.forEach((fn) => fn(args));

  const disable = () => {
    enabled = false;
  };

  return { connect, disconnect, emit, disable };
}) as SignalMaker;
