/**
 * Returns a formatted percent from a given fraction.
 * @param fraction any fractional number, e.g, 5/10
 * @param [pad] defaults to `false`
 */
export const percentFromProgress = (fraction: number, pad = false) =>
  (Math.round(fraction * 100) + '%').padStart(pad ? 3 : 0, '0')