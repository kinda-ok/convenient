/**
 *
 * Creates a throttled function that only invokes the provided function at most
 * once per within a given number of milliseconds.
 *
 * The first call and last calls are guaranteed, unless cancelled.
 *
 * Because the result is cached between calls, the received return may correspond to a previous call.
 * For that reason, it's better to not depend on any return value.
 * @param func
 * @param [firingRateMs] Firing rate (50ms by default)
 * @param [abortSignal]
 */
export const throttle = <R, P extends unknown[]>(func: (...args: P) => R, firingRateMs = 50, abortSignal?: AbortSignal): (...args: P) => R => {
  let lastResult: R;
  let last = 0;
  let funcArguments: P | null;
  let timeoutID: NodeJS.Timeout;
  let cancelled = false;

  const onTimeOut = () => {
    if (cancelled) {
      return;
    }
    clearTimeout(timeoutID);
    last = +new Date();
    // eslint-disable-next-line prefer-spread
    lastResult = func.apply(null, funcArguments as P);
    funcArguments = null;
  };

  /**
   *
   * @param  {P} args
   */
  const throttled = (...args: P) => {
    if (cancelled) {
      return lastResult;
    }
    funcArguments = args;
    const delta = new Date().getTime() - last;
    if (!timeoutID) {
      if (delta >= firingRateMs) {
        onTimeOut();
      } else {
        timeoutID = setTimeout(onTimeOut, firingRateMs - delta);
      }
    }
    return lastResult;
  };

  abortSignal?.addEventListener("abort", () => {
    cancelled = true;
    clearTimeout(timeoutID);
  });

  return throttled;
};
