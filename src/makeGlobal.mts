'use strict'


const rootObject: any =
  (typeof self === 'object' && self.self === self && self) ||
  (typeof global === 'object' && global.global === global && global) ||
  /** @type {globalThis} */(/** @type {unknown} */(this))

/**
 * Sets a property on the global object. That is `window` in browser environments, `process` in node, etc
 */
export const setGlobal = (name: string, value: unknown) =>
  (rootObject[name] = value)

/**
 * Retrieves a property from the global object (`window` in browser environments, `process` in node, etc)
 */
export const getGlobal = <T,>(name: string, defaultValue: T) => name in rootObject ? (rootObject[name] as T) : defaultValue
