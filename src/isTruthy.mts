import { isFalsy, type Falsy } from './isFalsy.mjs'

export const isTruthy = <T,>(thing: T | Falsy): thing is T => !isFalsy(thing)

