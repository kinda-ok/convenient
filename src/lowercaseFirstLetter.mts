import { isFalsy } from "./isFalsy.mjs";

export type LowerCasedFirstLetter = string

/**
 *
 * Lower cases the first letter of a string
 * or an array of strings.
 *
 *
 * If passed an array, the function will join them
 * with the `sep` argument.
 *
 * @param str
 * @param [sep] defaults to "."
 */
export const lowercaseFirstLetter = (str: (string | string[]), sep: string = "."): LowerCasedFirstLetter =>
  isFalsy(str)
    ? ""
    : Array.isArray(str)
    ? str
        .map((s) => lowercaseFirstLetter(s, sep))
        .filter(Boolean)
        .join(sep)
    : str.charAt(0).toLowerCase() + str.slice(1);
