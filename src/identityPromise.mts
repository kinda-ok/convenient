/**
 * Returns the passed object
 */
export const identityPromise = <T,>(value:T): Promise<T> => Promise.resolve(value)

export type PromisedIdentity = typeof identityPromise
