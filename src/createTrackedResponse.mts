import { deferredPromise, type DeferredPromise } from "./deferredPromise.mjs";


interface TrackedResponseProps{
	readonly start: (value: ReadableStreamDefaultReader<Uint8Array>) => void
	readonly response: Response
	readonly received: number
	readonly isStarted: boolean
	readonly isFailed: boolean
	readonly isSuccess: boolean
	readonly isUnknown: boolean
}

type TrackedResponse = Response & TrackedResponseProps

/**
 * Creates a response object that can dispatch progress.
 * @param onProgress
 * @param [signal]
 * @param [maxPackets] a maximum amount of packets to receive
 */
export const createTrackedResponse = (onProgress: (received: number) => void, signal?: AbortSignal, maxPackets: number = 99999) => {
  const readerPromise: DeferredPromise<ReadableStreamDefaultReader<Uint8Array>> = deferredPromise();
  const successPromise: DeferredPromise<Response> = deferredPromise();

  let received = 0;
  let started = false;
  let failed = false;
  let success = false;

  const response = new Response(
    new ReadableStream({
      async start(controller) {
        const onError = (/** @type {Error} */ error: Error) => {
          failed = true;
          success = false;
          controller.close();
          controller.error(error);
          successPromise.reject(error);
        };
        const onSuccess = () => {
          failed = false;
          success = true;
          successPromise.resolve(response);
        };
        signal &&
          signal.addEventListener("abort", () =>
            onError(new Error("Stream aborted"))
          );
        try {
          const reader = await readerPromise;
          started = true;
          try {
            while (true && maxPackets-- > 0) {
              const { done, value } = await reader.read();
              if (done) {
                controller.close();
                onProgress(received);
                break;
              }
              received += value.byteLength;
              controller.enqueue(value);
              onProgress(received);
            }
            onSuccess();
          } catch (error) {
            onError(error as Error);
          }
        } catch (readerError) {
          onError(readerError as Error);
        }
      },
    })
  );

  const start = readerPromise.resolve;

  return {
    start,
    then: successPromise.then.bind(successPromise),
    catch: successPromise.catch.bind(successPromise),
    get response() {
      return response;
    },
    get received() {
      return received;
    },
    get isStarted() {
      return started;
    },
    get isFailed() {
      return failed;
    },
    get isSuccess() {
      return success;
    },
    get isUnknown() {
      return success === false && failed === false;
    },
  };
}
