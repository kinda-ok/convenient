const _titleData = {
  pageTitle: '',
  siteTitle: '',
  template: '{2} | {1}'
}

const _updateTitle = () => {
  if (document == null) {
    return
  }

  const { pageTitle, siteTitle, template } = _titleData

  if (siteTitle === '') {
    document.title = pageTitle
  } else if (pageTitle === '') {
    document.title = siteTitle
  } else {
    document.title = template
      .replace(/\{1\}/, siteTitle)
      .replace(/\{2\}/, pageTitle)
  }
}

/**
 * Sets the page title and site title. If either is skiped, that part of the title will not be changed
 * @param [title] the title of the page. For example, "home"
 * @param [hostTitle] the title of the site. For example, "My Cool Site"
 */
export const changeTitle = (title?: string, hostTitle?: string) => {
  let changed = false
  if (title) {
    _titleData.pageTitle = title
    changed = true
  }
  if (hostTitle) {
    _titleData.siteTitle = hostTitle
    changed = true
  }
  if (changed) {
    _updateTitle()
  }
}


/**
 * the template to use, where {1} is the site title, and {2} is the page title
 * In the sense `{2} | {1}`
 */
export const titleTemplate = (new_template: string) => {
  _titleData.template = new_template
  _updateTitle()
}

export const title = {
  set page(new_title) {
    _titleData.pageTitle = new_title
  },
  get page() {
    return _titleData.pageTitle
  },
  set site(new_title) {
    _titleData.siteTitle = new_title
  },
  get site() {
    return _titleData.siteTitle
  },
  set template(new_template) {
    _titleData.template = new_template
  },
  get template() {
    return _titleData.template
  }
}
