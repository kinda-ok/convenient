
import { isEnvBrowser } from "./isEnvBrowser.mjs";

const cache: {[key:string]: true} = {};

const _loadScript = (url: string, bypassCache: boolean = false): Promise<string> =>
  new Promise((resolve, reject) => {
    if (!bypassCache && cache[url]) {
      resolve(url);
      return;
    }
    const onLoad = () => {
      cache[url] = true;
      resolve(url);
    };
    const script = document.createElement("script");
    script.type = "text/javascript";
    // for old IE
    if ("readyState" in script) {
      // @ts-ignore
      script["onreadystatechange"] = () => {
        if (
          script["readyState"] === "loaded" ||
          script["readyState"] === "complete"
          ) {
          // @ts-ignore
          script["onreadystatechange"] = null;
          cache[url] = true;
          onLoad();
        }
      };
      // for everything else
    } else {
      script.onload = onLoad;
      script.onerror = (/** @type {Event | string} */ evt: Event | string) => {
        if (typeof evt === "string") {
          reject(new Error(evt || "Could not load file"));
          return;
        }
        reject(new Error(evt.type + " Could not load file"));
      };
    }
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
  });

const fakeLoadScript: typeof _loadScript = (url) => Promise.resolve(url);

/**
 * loads an external script. Useful for loading google maps or other external APIs
 * On the server, this is a no-op
 */
export const loadScript = isEnvBrowser ? _loadScript : fakeLoadScript;
