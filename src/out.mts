//@ts-check
import { isDev } from './isDev.mjs'
import { identity } from './identity.mjs'

type OutLoggerMaker = typeof outLog

export const outLog = (title = '') => {
  /**
   * Log something to console
   */
  const out = <T,>(thing: T, ...things: unknown[][]) => (
    console.log(title, thing, ...things), thing
  )
  return out
}



export const out_stub: OutLoggerMaker = () => identity

/**
 * returns a logger
 * The logged logs everything passed to console, then returns the first passed element.
 * It is thus suitable to be used in promises, for example:
 * ```js
 * fetch('some/url')
 *  .then(out('initial'))
 *  .then(res=>res.json())
 *  .then(out('json'))
 *  .catch(out('err'))
 * ```
 * In production, all those calls are washed away and nothing logs.
 * This is not a replacement
 * @param title
 */
export const out = isDev ? outLog : out_stub


