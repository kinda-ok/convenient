import { isPrimitive } from "./isPrimitive.mjs";

/**
 * Returns true if the passed object implements the iterator interface
 */
export const isSyncIterator = (thing: unknown): thing is Iterable<unknown> =>
  !isPrimitive(thing) &&
  //@ts-expect-error TS isn't managing to discriminate on unknown, even though we eliminated primitive types
  Symbol.iterator in thing &&
  typeof thing[Symbol.iterator] === "function";

/**
 * Returns true if the passed object implements the async iterator interface
 */
export const isAsyncIterator = (thing: unknown): thing is AsyncIterableIterator<unknown> =>
  !isPrimitive(thing) &&
  //@ts-expect-error TS isn't managing to discriminate on unknown, even though we eliminated primitive types
  Symbol.asyncIterator in thing &&
  typeof thing[Symbol.asyncIterator] === "function";

/**
 * Returns true if the passed object implements either the async or sync iterator interface
 */
export const isIterator = (thing: unknown): thing is Iterable<unknown> | AsyncIterableIterator<unknown> =>
  isSyncIterator(thing) || isAsyncIterator(thing);
