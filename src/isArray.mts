
/**
 * Checks if the passed object is an array
 */
export const isArray = (obj: unknown): obj is unknown[] => !!obj && Array.isArray(obj);
