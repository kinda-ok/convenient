import { parseJson } from "./parseJson.mjs";

/**
 *
 */
const hasErrorAndMessage = (data: unknown): data is { error: { message: string; }; } =>
  data &&
  typeof data === "object" &&
  "error" in data &&
  data.error &&
  typeof data.error === "object" &&
  "message" in data.error &&
  data.error.message &&
  typeof data.error.message === "string"
    ? true
    : false;

/**
 * Creates an equivalent to the native fetch,
 * but that dispatches events on error, success, and loading
 * it also caches responses
 * @param [_fetch]
 */
export const fetch_dispatcher = <T extends JsonObject, E extends JsonObject>(_fetch: Fetch = fetch) => {
  const successListeners: FetchPatcherListener<FetchPatcherEventSuccess<T>>[] = [];
  const errorListeners: FetchPatcherListener<FetchPatcherEventError<E>>[] = [];
  const loadingListeners: FetchPatcherListener<FetchPatcherEventLoad>[] = [];
  const anyListeners: FetchPatcherListener<FetchPatcherEventLoad>[] = [];
  const cache = new Map();
  
  const dispatch = <F extends FetchPatcherEvent<T,E>>(arr: FetchPatcherListener<F>[]) => (evt: F) => {
    arr.forEach((l) => l(evt));
    anyListeners.forEach((l) => l(evt));
    return evt;
  };
  const dispatchSuccess = dispatch(successListeners);
  const dispatchError = dispatch(errorListeners);
  const dispatchLoading = dispatch(loadingListeners);
  const addEventListener =
    
    <E extends FetchPatcherEvent<T,E>>(arr: FetchPatcherListener<E>[]) => (listener: FetchPatcherListener<E>) => {
      arr.push(listener);
      const removeEventListener = () => {
        const index = arr.indexOf(listener);
        if (index >= 0) {
          arr.splice(index, 1);
        }
      };
      return removeEventListener;
    };
  const onSuccess = addEventListener(successListeners);
  const onError = addEventListener(errorListeners);
  const onLoad = addEventListener(loadingListeners);
  const NoURLError = new Error(`no URL provided`);
  const fetch_and_dispatch =
    (request: Request, init: RequestInit) => {
      dispatchLoading({ type: "loading", request });
      return _fetch(request, init)
        .then(({ text, statusText, status }) =>
          text()
            .then(json => parseJson(json))
            .then((data) =>
              status >= 400 && status <= 599
                ? dispatchError({
                    type: "error",
                    request,
                    data: (data as E),
                    error: new Error(statusText),
                  })
                : hasErrorAndMessage(data)
                ? // tslint:disable-next-line:max-line-length
                  dispatchError({
                    type: "error",
                    request,
                    data: (data as unknown as E),
                    error: new Error(data.error.message),
                  })
                : dispatchSuccess({
                    type: "success",
                    request,
                    data: (data as T),
                  })
            )
        )
        .then((evt) => {
          if (evt.type === "error") {
            throw new Error(evt.error.message);
          }
          return evt.data;
        })
        .catch((error) =>
          dispatchError({
            type: "error",
            request,
            error,
            data: (null as E),
          })
        );
    };
  const fetch_and_cache =
    (request: Request | string, init: RequestInit & { invalidate?: boolean; }) => {
      if (!request) {
        return Promise.reject(NoURLError);
      }
      if (!(request instanceof Request)) {
        request = new Request(request);
      }
      const { url, method } = request;
      const key = method + ":" + url;
      if (init && init.invalidate) {
        cache.delete(key);
      }
      if (!cache.has(key)) {
        const promise = fetch_and_dispatch(request, init);
        cache.set(key, promise);
      }
      return cache.get(key);
    };
  const methods = { onSuccess, onError, onLoad, load: fetch_and_dispatch };
  const final = Object.assign(fetch_and_cache, methods);
  return final;
};
