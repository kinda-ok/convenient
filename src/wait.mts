/**
 * Waits the specified amount of time before returning the value.
 * Can be used as an identity function.
 * @param [durationMs] Duration, in milliseconds. Defaults to 1 second
 */
export const wait =
  ( durationMs = 1_000 ) =>
  <T,>( value: T ) =>
    new Promise<T>(
      ( resolve ) => 
      ( setTimeout(resolve, durationMs, value)
      )
    );
