// @ts-check
import { getHTMLFirstTitleContents } from "./getHTMLFirstTitleContents.mjs";
import { generateDomFromString } from "./generateDomFromString.mjs";

/**
 * Takes a an original string and transforms it into dom that can be slotted in a
 * document.
 * Additionally, it parses the frontmatter, and attempts to extract a title by finding either
 * the first title in the doc, or the filename (if provided).
 *
 * This is primarily there to allow to pass a markdown parser and get html, frontmatter, and
 * a title back.
 * @param transformer
 */
export const strTransformedToDom =
  (transformer: (rawStr: string) => { data: Record<string, unknown>; markup: string; }) =>
  (rawStr: string, path: string = "") => {
    const { data, markup } = transformer(rawStr);
    const content = generateDomFromString(markup);
    const title =
      getHTMLFirstTitleContents(content) || path.replace(/\.\w{2, 4}$/, "");
    return { title, raw: rawStr, content, data };
  };
