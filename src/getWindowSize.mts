import { isEnvBrowser } from './isEnvBrowser.mjs'

const server_size = { width: 0, height: 0 }

/**
 * Returns the window size.
 * On the server, returns width and height 0
 */
export const getWindowSize = isEnvBrowser
  ? () => {
      const width =
        window.innerWidth ||
        document.documentElement.clientWidth ||
        document.body.clientWidth
      const height =
        window.innerHeight ||
        document.documentElement.clientHeight ||
        document.body.clientHeight
      return { width, height }
    }
  : () => server_size
