//@ts-check
import { getFilePathExtension } from "./getFilePathExtension.mjs";
import { isEnvBrowser } from "./isEnvBrowser.mjs";
import { loadImage, type ImageLoadReturn, ImageLoadReturnJSON } from "./loadImage.mjs";

export interface ImageReadReturnJSON{
	name: string
	extension: string
}

interface ImageReadReturnNoImage extends ImageReadReturnJSON{
	file: File
	toJSON: () => ImageReadReturnJSON
}

interface ImageReadReturn extends ImageLoadReturn{
	free: () => void
	file: File
	name: string
	extension: string
	toJSON: () => ImageReadReturnJSON & ImageLoadReturnJSON
}


const serverResponse: ImageReadReturnNoImage = {
  file: ({} as File),
  name: "",
  extension: "",
  toJSON: () => ({ name: "", extension: "" }),
};


export const isImageFile = (file: File, extension: string) =>
  file &&
  ((file.type && file.type.split("/")[0] === "image") ||
    /gif|png|jpe?g|tiff?|webp|bmp|ico|svg/.test(extension));


export const _readImageFromFile = (file: File, isImage: (file: File, extension: string) => boolean, useDecode: boolean = true): Promise<ImageReadReturnNoImage | ImageReadReturn> => {
  if (typeof isImage === "boolean" && isImage === true) {
    isImage = isImage;
  }
  return new Promise((resolve, reject) => {
    if (!file) {
      reject(new Error("no file provided"));
      return;
    }
    const name = file.name;
    const extension = getFilePathExtension(name);
    if (isImage && !isImage(file, extension)) {
      const ret = {
        file,
        name,
        extension,
        toJSON: () => ({ name, extension }),
      };
      resolve(ret);
      return;
    }
    window.URL = window.URL || window["webkitURL"];
    const src = window.URL.createObjectURL(file);
    const free = () => {
      window.URL.revokeObjectURL(src);
    };
    loadImage(src, useDecode && extension !== "svg")
      .then(({ toJSON: prevToJSON, ...props }) => {
        const ret = {
          ...props,
          free,
          file,
          name,
          extension,
          toJSON: () => ({ ...prevToJSON(), name, extension }),
        };
        resolve(ret);
      })
      .catch(reject);
  });
};


const _fakeReadImageFromFile: typeof _readImageFromFile = () => Promise.resolve(serverResponse);

/**
 * If the provided file is an image, this function will load the image
 * and return a set of useful properties.
 * If not, it will return the file, with extracted name and extension strings
 *
 * This function has a custom toJSON method that removes non-serializable data
 * @param file
 * @param [isImage] a function that receives the file and the extension, and has
 * to return a boolean if the provided file is an image. If `true` is passed, a default function will be used,
 * which checks for mime-type that begins with `image/` and/or most common extensions (gif/png/jpg...)
 * @param [useDecode] if true, will use `img.decode()` to prevent the browser from slowing down while loading the image
 */
export const readImageFromFile = isEnvBrowser
  ? _readImageFromFile
  : _fakeReadImageFromFile;
