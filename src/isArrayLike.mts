import { isArray } from "./isArray.mjs";

/**
 * Checks if the passed object is iterable
 */
export const isArrayLike = (obj: unknown): obj is ArrayLike<unknown> =>
  isArray(obj) ||
  (typeof obj === "object" &&
    obj !== null &&
    "length" in obj &&
    typeof obj.length === "number" &&
    (obj.length === 0 || (obj.length > 0 && obj.length - 1 in obj)));
