/**
 * Tries to turn anything into an error 
 */
export const dataToError = (src: any) => {
  if(src instanceof Error){
    return src
  }
  if(typeof src === 'object' && 'error' in src){
    const error = src.error
    if(typeof error === 'object' && 'message' in error){
      if(typeof error.message === 'string'){
        return new Error(error.message)
      }
      return new Error(JSON.stringify(error.message))
    }
    return new Error(JSON.stringify(error))
  }
  return new Error(JSON.stringify(src))
}