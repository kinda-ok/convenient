//@ts-check
import { isEnvBrowser } from "./isEnvBrowser.mjs";
import {
  getRectangleOrientation,
  ORIENTATION_SQUARE,
	type Orientation
} from "./getRectangleOrientation.mjs";


export interface ImageLoadReturnJSON{
	width: number
	height: number
	url: string
	ratioWidth: number
	ratioHeight: number
	orientation: Orientation
}

export interface ImageLoadReturnProps{
	image: HTMLImageElement
	toJSON: () => ImageLoadReturnJSON
}

export interface ImageLoadReturn extends ImageLoadReturnProps, ImageLoadReturnJSON{}


const serverResponseJSON = {
  width: 0,
  height: 0,
  url: "",
  ratioWidth: 1,
  ratioHeight: 1,
  orientation: ORIENTATION_SQUARE,
};

const serverResponse: ImageLoadReturn = {
  image: ({} as HTMLImageElement),
  ...serverResponseJSON,
  toJSON: () => serverResponseJSON,
};

/**
 *
 * @param src
 * @param [useDecode] if true, will use `img.decode()` to prevent the browser from slowing down while loading the image. If the image has the `decoding` property set to `async`, this is automatically true.
 */
const _loadImage = (src: string, useDecode: boolean = true): Promise<ImageLoadReturn> =>
  new Promise((resolve, reject) => {
    const image = new Image();

    const onload = () => {
      const { naturalHeight: height, naturalWidth: width } = image;
      const orientation = getRectangleOrientation(width, height);
      const ratioWidth = height / width;
      const ratioHeight = width / height;
      const url = image.src;
      const json = {
        width,
        height,
        url,
        ratioWidth,
        ratioHeight,
        orientation,
      };
      const ret: ImageLoadReturn = { image, ...json, toJSON: () => json };
      clean();
      resolve(ret);
    };

    const onerror = (evt: Event | string) => {
      clean();
      if (typeof evt === "string") {
        reject(new Error(evt || "could not load file"));
        return;
      }
      reject(new Error(evt.type + " could not load file"));
    };

    const clean = () => {
      image.onerror = null;
      image.onload = null;
    };

    image.onerror = onerror;
    image.src = src;

    if (useDecode || image.decoding === "async") {
      image.src = src;
      image.decode().then(onload).catch(onerror);
    } else {
      image.onload = onload;
    }
  });


const fakeLoadImage: typeof _loadImage = (_src, _useDecode = true) =>
  Promise.resolve(serverResponse);

/**
 * returns a promise with a loaded image and a few useful parameters,
 * such as width, height, ratio, and orientation
 *
 * This function has a custom toJSON method that removes non-serializable data
 *
 * On the server, a fake payload is returned, with proper properties but bogus data
 *
 * @param src
 * @param [useDecode] if true, will use `img.decode()` to prevent the browser from slowing down while loading the image. If the image has the `decoding` property set to `async`, this is automatically true.
 */
export const loadImage = isEnvBrowser ? _loadImage : fakeLoadImage;
