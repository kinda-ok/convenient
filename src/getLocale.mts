

export type uppercase = |'A'|'B'|'C'|'D'|'E'|'F'|'G'|'H'|'I'|'J'|'K'|'L'|'M'|'N'|'O'|'P'|'Q'|'R'|'S'|'T'|'U'|'V'|'W'|'X'|'Y'|'Z'
export type lowercase = |'a'|'b'|'c'|'d'|'e'|'f'|'g'|'h'|'i'|'j'|'k'|'l'|'m'|'n'|'o'|'p'|'q'|'r'|'s'|'t'|'u'|'v'|'w'|'x'|'y'|'z'
export type LanguageCode = `${lowercase}${lowercase}`
export type CountryCode = `${uppercase}${uppercase}`
export type Locale = [LanguageCode, CountryCode]
export type NoLocale = ['', '']

/**
 * Retrieves the current locale. Only works if `navigator` is available.
 * Otherwise, returns the `defaultLang` passed property
 * @param [defaultLang] defaults to an empty string
 * @returns The browser locale, formatted as [`xx`,`XX`], like: [`en`,`US`]
 */
export const getLocale = (defaultLang: Locale | NoLocale = ['','']): Locale | NoLocale => {
  if (typeof navigator === "undefined") {
    return defaultLang;
  }
  const navigatorLanguage = navigator.languages
    ? navigator.languages[0]
    : navigator.language;
  return (navigatorLanguage.split(".")[0].split('-')) as Locale
}
