//@ts-check

export type SnakeCasedString = string

/**
 *
 * Turns a string or an array of string into a snake_cased string.
 * "_"" are created for every capital letter encountered.
 * If there's a leading "_", it will be removed.
 *
 * If passed an array, it will be joined with the `sep` argument.
 *
 * @param str
 * @param [sep] defaults to `_`
 */
export const toSnakeCase = (str: (string | string[]), sep: string = "_"): SnakeCasedString =>
  Array.isArray(str)
    ? str.map((s) => toSnakeCase(s, sep)).join(sep)
    : str
        .replace(/\.?([A-Z]+)/g, (_, /** @type {string} */capture: string) => "_" + capture.toLowerCase())
        .replace(/^_/, "")
        .toUpperCase();
