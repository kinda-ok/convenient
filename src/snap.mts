//@ts-check


export enum SNAP_TYPE{
	MID,
	MIN,
	MAX
}

/**
 * Snaps to the rounded closest number
 * @param gridCellSize the grid size
 * @param num the current number to snap
 */
export const snapMid = (gridCellSize: number, num: number): number =>
  Math.round(num / gridCellSize) * gridCellSize;

/**
 * Snaps to the lowest closest number
 * @param gridCellSize the grid size
 * @param cursor the current number to snap
 */
export const snapMin = (gridCellSize: number, cursor: number): number =>
  Math.floor(cursor / gridCellSize) * gridCellSize;

/**
 * Snaps to the highest closest number
 * @param gridCellSize the grid size
 * @param cursor the current number to snap
 */
export const snapMax = (gridCellSize: number, cursor: number): number =>
  Math.ceil(cursor / gridCellSize) * gridCellSize;

/**
 * Returns the the nearest grid resolution less than or equal to the number.
 *
 * @param gridCellSize The grid resolution to snap to.
 * @param cursor The cursor to align.
 * @param snappingType snapping type (defaults to `mid`)
 * @returns The nearest multiple of resolution lower than the number.
 */
export const snap = (gridCellSize: number, cursor: number, snappingType: SNAP_TYPE = SNAP_TYPE.MID) =>
  snappingType === SNAP_TYPE.MAX
    ? snapMax(gridCellSize, cursor)
    : snappingType === SNAP_TYPE.MIN
    ? snapMin(gridCellSize, cursor)
    : snapMid(gridCellSize, cursor);
