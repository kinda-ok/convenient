import { isArrayLike } from "./isArrayLike.mjs";
import { isObject } from "./isObject.mjs";
import { isIterator } from "./isIterator.mjs";

export type Traversable = | ArrayLike<unknown>| Iterable<unknown>| AsyncIterableIterator<unknown>| Record<string, unknown>| Record<symbol, unknown>| Record<number, unknown>| Map<unknown, unknown>| Set<unknown>| string

export const isTraversable = (thing: unknown): thing is Traversable =>
  typeof thing !== "undefined" &&
  thing !== null &&
  (isArrayLike(thing) ||
    thing instanceof Map ||
    thing instanceof Set ||
    isObject(thing) ||
    isIterator(thing));
