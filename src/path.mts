
export const filename = (str: string, sep: string = "/") => str.slice(str.lastIndexOf(sep) + 1);

export const stripExtension = (str: string) => str.slice(0, str.lastIndexOf("."));

export const basename = (str: string, sep: string = "/") => stripExtension(filename(str, sep));


export const extension = (str: string) => str.slice(str.lastIndexOf(".") + 1);

export const dirName = (str: string, sep: string = "/") =>
  str.slice(0, str.lastIndexOf(sep) + 1);

/**
 * @param str
 * @param [sep] defaults to `/`
 * @returns
 */
export const metadata = (str: string, sep: string = "/") => ({
  basename: basename(str, sep),
  filename: filename(str, sep),
  extension: extension(str),
  dirName: dirName(str),
  fullPath: str,
});
