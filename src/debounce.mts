//@ts-check

/**
 * Ensures that the passed function gets called only once even if it runs multiple times under a specified delay
 * Returns a function, that, as long as it continues to be invoked, will not
 * be triggered. The function will be called after it stops being called for
 * N milliseconds. If `immediate` is passed, trigger the function immediately and cancels subsequent calls
 * @param func
 * @param [delay] defaults to 300 ms
 * @param [immediate]
 */
export const debounce = <P extends unknown[]>(
  func: (...args: P) => unknown,
  delay: number = 300,
  immediate: boolean = false
) => {
  let timeout: NodeJS.Timeout

  const debounced: (...args: P) => void = (...args): void => {
    const debounced_container = () => {
      clearTimeout(timeout)
      if (!immediate) {
        func(...args)
      }
    }
    const callNow = immediate && !timeout
    clearTimeout(timeout)
    timeout = setTimeout(debounced_container, delay)
    if (callNow) {
      func(...args)
    }
  }
  return debounced
}


