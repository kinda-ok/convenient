import { noOp } from "./noOp.mjs";

type Resolve<T> = (value?: T | PromiseLike<T>) => void
type Reject<T> = (reason?: T) => void

interface DeferredPromiseProps<T,E = any>{
	resolve: Resolve<T>,
	reject: Reject<E>
}

export type DeferredPromise<T, E = any> = Promise<T> & DeferredPromiseProps<T,E>


interface DeferredPromiseExecutor<T, E= any>{
	(resolve: Resolve<T>, reject: Reject<E>): void
}


/**
 * Returns a promise with exposed `resolve()` and `reject()` that can
 * be used any time.
 * Works well with lazy evaluation.
 * @param [executor] A function to execute immediately
 */
export const deferredPromise = <T, E = any>(executor: DeferredPromiseExecutor<T, E> = noOp): DeferredPromise<T, E> => {
  let receiver;
  const promise = new Promise((resolve, reject) => {
    receiver = { resolve, reject };
		//@ts-ignore
    executor(resolve, reject);
  }) as DeferredPromise<T>;
  Object.assign(promise, receiver);
  return promise;
};
