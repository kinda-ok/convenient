import { basename } from "./path.mjs";

export type keyword = string
export type abbreviation = string
export type description = string

interface ParsedArgs{
	executable: string
	path: string
	file: string
	flags: Record<string, Array<string|boolean>>
	rest: string[]
}



/**
 * Parses a string of arguments. Handles duplicates.
 * A second object can be passed to expand parameters.
 * @param args expects to start with the executable and path, so the first two items will be discarded
 */
export const readArgs = (args: string[], expand: Record<abbreviation, keyword>) =>
  args.slice(2).reduce(
    (acc, str) =>
    {
    ; if (!str.startsWith("-")) 
      { acc.rest.push(str)
      } 
      else 
      { const 
        { dashes
        , isNegated
        , key
        , val 
        } = str.match(
          /(?<dashes>-+)(?<isNegated>no-)?(?<key>[^=]*)(?:=(?<val>.*))?/
        )?.groups || 
        { dashes: "-"
        , isNegated: ""
        , key: str
        , val: "" 
        }
      ; const keyword = (
          dashes.length > 0 && expand && key in expand 
          ? expand[key]
          : key
        )
      ; const value = ( 
          isNegated
          ? false
          : typeof val === 'undefined' || val === ""
            ? true
            : val.toLowerCase() === "true"
              ? true
              : val.toLowerCase() === "false"
                ? false
                : val
        )
      ; if (!(keyword in acc.flags))
        { acc.flags[keyword] = []
        }
      ; acc.flags[keyword].push(value)
      }
    ; return acc
    }
  , (
    { executable: args[0]
    , path: args[1]
    , file: basename(args[1])
    , flags: {}
    , rest: []
    } as ParsedArgs)
  )