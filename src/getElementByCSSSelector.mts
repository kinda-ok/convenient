//@ts-check
import { isLocalhost } from "./isLocalhost.mjs";

/**
 * Gets an element by a valid selector if the element exists, otherwise throws, but only if running in localhost environments.
 * Use this in the initial setup to verify all elements exist
 * @param selector
 */
export const getElementByCSSSelector = (selector: string) => {
  const element: HTMLElement | null = document && document.querySelector(selector);
  if (isLocalhost && element == null) {
    throw new Error(`Element "#${selector}" was not found`);
  }
  return element;
};
