/**
 * Verifies an element is actually an element.
 */
export const isElement = (element: unknown): element is HTMLElement =>
  element instanceof Element || element instanceof Document;
