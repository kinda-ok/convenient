/**
 * Makes sure each element in an array is unique. Only works for scalars
 */
export const ensureUniqueArray = 
  <T,>( arr: T[] ) => 
  [ ...new Set(arr) ]