import { isLocalhost } from "./isLocalhost.mjs";

/**
 * Gets an element by id if the element exists, otherwise throws, but only if running in localhost environments.
 * Use this in the initial setup to verify all elements exist
 * @param id
 */
export const getElementById = (id: string) => {
  const element: HTMLElement | null = document && document.getElementById(id);
  if (isLocalhost && element == null) {
    throw new Error(`Element "#${id}" was not found`);
  }
  return element;
}
