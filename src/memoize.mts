

/**
 * Picks the first argument and stringifies it
 */
export const pickFirstArgument = <T extends (...args: any[]) => any>(...args: Parameters<T>) => JSON.stringify(args[0]);

/**
 * Caches the result of a function.
 * The cache is available as `.cache` in case there's a need to clear anything.
 * Uses the first parameter as a key by default, you can change this behavior by passing a custom
 * hash function.
 */
export const memoize = <T extends (...args: any[]) => any>(
  functionToMemoize: T,
  hashFunction: (...args: Parameters<T>) => string | number = pickFirstArgument
) => {
  const cache: Map<string | number, ReturnType<T>> = new Map();
  
  const memoized = (...args: Parameters<T>): ReturnType<T> => {
    const key = hashFunction(...args);
    const result = functionToMemoize(...args);
    if (!cache.has(key)) {
      cache.set(key, result);
    }
    return result;
  };

  return Object.assign(memoized, { cache });
}
