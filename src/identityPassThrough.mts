
/**
 * Wraps a function to return the initially passed argument after running.
 * Compatible with both sync and async functions.
 * You can safely pass falsy values.
 * If you pass a falsy value in the place of the function, then the result is a no-op.
 */
export const identityPassThrough = 
  <T,>(fn: () => Promise<void> | void) => 
  (thing: T): T | Promise<T> => 
  {
  ; if (!fn) 
    {
    ; return thing
    }
  ; const res = fn()
  ; if (res && 'then' in res) 
    {
    ; return res.then(
        () => thing
      )
    }
  ; return thing
  }