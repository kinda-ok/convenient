//@ts-check

type KebabCasedString = string

/**
 *
 * If passed an array, it will be joined with the `sep` argument.
 *
 * @param str
 * @param [sep] defaults to `-`
 */
export const toKebabCase = (str: string| string[], sep = "-"): KebabCasedString =>
  Array.isArray(str)
    ? str.map((s) => toKebabCase(s, sep)).join(sep)
    : str
        .replace(/([a-z])([A-Z])/g, `$1-$2`)
        .replace(/[\s_]+/g, '-')
        .toLowerCase();
