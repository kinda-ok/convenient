import { isAsyncIterator, isIterator } from "./isIterator.mjs";
import { isArrayLike } from "./isArrayLike.mjs";
import { BREAK, SKIP } from "./symbols.mjs";
import type { Traversable } from "./isTraversable.mjs";


export const traverseArrayLike = <V,>(obj: ArrayLike<V>, observer: TraversableObserver<ArrayLike<V>, number, V>) => {
  const { length } = obj;
  let key = 0;
  for (; key < length; key++) {
    const value = obj[key];
    const result = observer(value, key, obj, BREAK);
    if (result === BREAK) {
      return;
    }
  }
};

export const traverseString = (obj: string, observer: TraversableObserver<string, number, string>) =>{
  const { length } = obj;
  let key = 0;
  for (; key < length; key++) {
    const value = obj[key];
    const result = observer(value, key, obj, BREAK);
    if (result === BREAK) {
      return;
    }
  }
};

export const traverseIteratable = <V,>(obj: Iterable<V>, observer: TraversableObserver<Iterable<V>, number, V>) => {
  let key = 0;
  for (const value of obj) {
    const result = observer(value, key, obj, BREAK);
    key = key + 1;
    if (result === BREAK) {
      return;
    }
  }
};

export const traverseAsyncIteratable = async <V,>(obj: AsyncIterableIterator<V>, observer: TraversableObserver<AsyncIterableIterator<V>, number, V>) => {
  let key = 0;
  for await (const value of obj) {
    const result = observer(value, key, obj, BREAK);
    key = key + 1;
    if (result === BREAK) {
      return;
    }
  }
};


export const traverseMap = <K, V>(obj: Map<K, V>, observer: TraversableObserver<Map<K, V>, K, V>) => {
  for (const [key, value] of obj) {
    const result = observer(value, key, obj, BREAK);
    if (result === BREAK) {
      return;
    }
  }
};


export const traverseObject = <V,>(obj: Record<string, V>, observer: TraversableObserver<object, string, V>) => {
  const keys = Object.keys(obj);
  const { length } = keys;
  let index = 0;
  for (; index < length; index++) {
    const key = keys[index];
    const value = obj[key];
    const result = observer(value, key, obj, BREAK);
    if (result === BREAK) {
      return;
    }
  }
};
