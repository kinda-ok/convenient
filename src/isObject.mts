/**
 *
 * Verifies the passed object is of the 'object' family.
 *
 * Does not verify if the passed object is a plain object,
 * so `Date`s and other class instances will return true.
 *
 * However `null` will return false
 */
export const isObject = (thing: unknown): thing is object => Boolean(thing) && typeof thing == "object";
